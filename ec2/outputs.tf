output "public_ip" {
  value       = module.ec2.public_ip
  description = "eip created"
  depends_on  = []
}

output "Rundeck_volume" {
  value = module.ec2.Rundeck_volume 
}