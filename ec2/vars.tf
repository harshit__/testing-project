variable "aws_region" {
  type        = string
  default     = "ap-south-1"
  description = "description"
}

variable "ami" {
  type        = string
  default     = "ami-06a0b4e3b7eb7a300"
  description = "description"
}
variable "instance_type_server" {
  type        = string
  default     = "t2.medium"
}

variable "instance_type_node" {
  type        = string
  default     = "t2.micro"
}

variable "instance_tag" {
  type        = string
  default     = "RundeckServer"
}

variable "key_name_server" {
  type        = string
  default     = "RundeckServer"
  description = "RundeckServerKey"
}

variable "key_name_node" {
  type        = string
  default     = "NodeServer"
  description = "RundeckServerKey"
}

variable "server_name" { 
  type = string
  default = "RundeckServer"
}

variable "node_name" { 
  type = list(any)
  default = [ "Node1", "Node2", "node3"]
}

variable "region" {
    type = string
    default = "ap-south-1" 
}

variable "eip_name" {
    type = string
    default = "RundeckServerEip12" 
}

variable "Rundeck_volume" {
  type = string
  default = "RundeckStorage"
}

