data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = "resourcestatemanagement"
    key    = "vpc/terraform.tfstate"
    region = "ap-south-1"
  }
}

data "terraform_remote_state" "sg" {
  backend = "s3"
  config = {
    bucket = "resourcestatemanagement"
    key    = "sg/terraform.tfstate"
    region = "ap-south-1"
  }
}



module "ec2" {
  source               = "../modules/ec2"
  eip_name             = "${var.eip_name}"
  Rundeck_volume       = "${var.Rundeck_volume}"
  subnet_id_public     = "${element(data.terraform_remote_state.vpc.outputs.pub_sub_id,0)}"
  subnet_id_private    = "${element(data.terraform_remote_state.vpc.outputs.pri_sub_id,0)}"
  security_group_rundeck = data.terraform_remote_state.sg.outputs.sg_id_rundeck
  security_group_nodes   = data.terraform_remote_state.sg.outputs.sg_id_nodes
  ami                  = var.ami
  key_name_server      = var.key_name_server
  key_name_node        = var.key_name_node
  instance_type_server = var.instance_type_server
  instance_type_node   = var.instance_type_node
  server_name          = var.server_name
  node_name            = var.node_name
}

terraform {
  backend "s3" {
    bucket = "resourcestatemanagement"
    key    = "ec2/terraform.tfstate"
    region = "ap-south-1"
  }
}
