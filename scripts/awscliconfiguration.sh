#!/bin/bash

apt-get install awscli -y 

aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID && aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY && aws configure set default.region $AWS_DEFAULT_REGION

aws s3 cp s3://resourcestatemanagement/ec2/terraform.tfstate /builds/harshit__/testing-project/ec2/details.json