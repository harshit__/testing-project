#!/bin/bash

sudo yum install epel-release -y

sudo yum install dnf -y

sudo yum -y install python3-pip

sudo pip3 install --upgrade pip 

sudo yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm

# sudo yum update -y

# wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# sudo yum install epel-release-latest-7.noarch.rpm -y 

# sudo yum update -y

# sudo yum install git python python-devel python-pip openssl ansible -y


sudo yum install  --enablerepo epel-playground  ansible -y 

sudo cp -r /tmp/playbooks/ /etc/ansible/

sudo chmod 755 /etc/ansible/ansible.cfg && sudo chmod 755 /etc/ansible/hosts

sudo sed -i 's/#inventory/inventory/g' /etc/ansible/ansible.cfg
sudo sed -i 's/#host_key_checking/host_key_checking/g' /etc/ansible/ansible.cfg



