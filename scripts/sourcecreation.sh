#!/bin/bash


cd s3 && terraform init && terraform apply --auto-approve

cd .. && cd vpc && terraform init && terraform apply --auto-approve

cd .. && cd sg && terraform init && terraform apply --auto-approve

cd .. && cd ec2 && terraform init && terraform apply --auto-approve


