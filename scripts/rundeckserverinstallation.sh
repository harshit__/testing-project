#!/bin/bash

sudo curl https://raw.githubusercontent.com/rundeck/packaging/main/scripts/rpm-setup.sh 2> /dev/null | sudo bash -s rundeck -y 

sudo yum install java-11-openjdk -y

sudo yum install rundeck -y

cd /tmp/ && sudo cp /tmp/NodeServer.pem /var/lib/rundeck/.ssh/ && mv NodeServer.pem .secret

sudo chmod 755 /var/lib/rundeck/.ssh/NodeServer.pem && cd 

sudo /etc/init.d/rundeckd start

sudo systemctl enable rundeckd

public_ip=$(cat /tmp/public_ip.txt)

echo $public_ip

sudo sed -i "s/localhost/$public_ip/g" /etc/rundeck/framework.properties

sudo sed -i "s/localhost/$public_ip/g" /etc/rundeck/rundeck-config.properties

sudo systemctl restart rundeckd 

sudo bash /tmp/ansibleinstallation.sh 






