#!/bin/bash

apt-get install jq -y

public_ip=$(jq '.resources[5].instances[0].attributes.public_ip' /builds/harshit__/testing-project/ec2/details.json | tr -d '"')

chmod 600 RundeckServer.pem

ssh-keyscan -t rsa $public_ip  >> ~/.ssh/known_hosts

echo $public_ip >> public_ip.txt 

echo $public_ip && pwd && ls

chmod 644 ~/.ssh/known_hosts

scp -r -i RundeckServer.pem  public_ip.txt ec2-user@$public_ip:/tmp/

scp -r -i RundeckServer.pem  /builds/harshit__/testing-project/playbooks ec2-user@$public_ip:/tmp/

scp -r -i RundeckServer.pem  /builds/harshit__/NodeServer.pem ec2-user@$public_ip:/tmp/

scp -r -i RundeckServer.pem  /builds/harshit__/testing-project/scripts/rundeckserverinstallation.sh ec2-user@$public_ip:/tmp/

scp -r -i RundeckServer.pem  /builds/harshit__/testing-project/scripts/ansibleinstallation.sh ec2-user@$public_ip:/tmp/

TERM=xterm ssh -tt -o StrictHostKeyChecking=no  -i  RundeckServer.pem ec2-user@$public_ip bash /tmp/rundeckserverinstallation.sh