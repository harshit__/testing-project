variable "aws_region" {
  type        = string
  default     = "us-east-1"
  description = "description"
}

variable "subnet_list_rundeck" {
  type        = list(any)
  description = "description"
  default     = ["10.0.1.0/24"]
}

variable "subnet_list_nodes" {
  type        = list(any)
  description = "description"
  default     = ["10.0.2.0/24"]
}

variable "cidrs_rundeck" {
  type        = list(any)
  default     = ["157.38.19.59/32", "10.0.0.0/16", "0.0.0.0/0"]
}
variable "cidrs_nodes" {
  type        = list(any)
  default     = ["10.0.1.0/24"]
}

variable "tcp_ports_rundeck" {
    type =  list(any)
    default =  ["22", "4440"]
}

variable "tcp_ports_nodes" {
    type =  list(any)
    default =  ["22"]
}
variable "region" {
    type = string
    default = "ap-south-1" 
}

variable "sg_name_rundeck" {
  type = string
  default = "Rundeck"
}

variable "sg_name_nodes" {
  type = string
  default = "nodes"
}