data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = "resourcestatemanagement"
    key    = "vpc/terraform.tfstate"
    region = "ap-south-1"
  }
}

module "sg" {
  source       = "../modules/sg/"
  sg_name_rundeck       = var.sg_name_rundeck
  sg_name_nodes       = var.sg_name_nodes 
  vpc_id       = data.terraform_remote_state.vpc.outputs.vpc_id
}

module "sg_ingress" {
  source            = "../modules/sg_rules/"
  tcp_ports_rundeck         = var.tcp_ports_rundeck
  tcp_ports_nodes         = var.tcp_ports_nodes
  security_group_id_rundeck = element(module.sg.sg_id_rundeck, 0)
  security_group_id_nodes = element(module.sg.sg_id_nodes, 0)
  cidrs_rundeck = var.cidrs_rundeck
  cidrs_nodes = var.cidrs_nodes
}

terraform {
  backend "s3" {
    bucket = "resourcestatemanagement"
    key    = "sg/terraform.tfstate"
    region = "ap-south-1"
  }
}