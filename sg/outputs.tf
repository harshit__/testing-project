output "sg_id_rundeck" {
  value       = module.sg.sg_id_rundeck
  sensitive   = true
  description = "description"
  depends_on  = []
}

output "sg_id_nodes" {
  value       = module.sg.sg_id_nodes
  sensitive   = true
  description = "description"
  depends_on  = []
}


