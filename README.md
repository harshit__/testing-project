- This is the case study task:
    
    About -
        Created 3 servers on AWS cloud, one is Rundeck server and other two's are node servers. Rundeck server is used to manage multiple servers(nodes) using web ui and it serve multiple features like scheduling jobs, notification, access control to the user's etc. 
    
    Tools used -  
        - AWS
        - Terraform
        - GitLab & Gitlab Runner
        - Rundeck
