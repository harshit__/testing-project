module "vpc" {
source = "../modules/vpc"
  cidr_block       = var.cidr_block
  name             = var.name
  tags = {
    Name   = "${var.name}"
  }
}

module "subnet"{
source = "../modules/subnet"
vpc_id = module.vpc.vpc_id
pub_subnets = [
    {
        subnet_cidr = "10.0.1.0/24"
        availability_zone = "ap-south-1a"
        name = "${var.pub_name}"

    }
]
pri_subnets = [
    {
        subnet_cidr = "10.0.2.0/24"
        availability_zone = "ap-south-1b"
        name = "${var.pri_name}"

    }
]
}

module "ig"{
source = "../modules/ig"  
vpc_id = module.vpc.vpc_id
ig_name = "${var.ig_name}"
tags = {
    Name = "${var.ig_name}"
  }
}

module "nat"{
source = "../modules/nat"
eip_name = "${var.eip_name}"
ng_name = "${var.ng_name}"
subnet_id = element(module.subnet.public_subnet_ids, 0)
eip_tags = {
    Name = "${var.eip_name}"
  }
nat_tags = {
    Name = "${var.ng_name}"
  }  
}


module "route_table"{
source = "../modules/route_table"
for_each =  { 
                0 = {       cidr_block = "0.0.0.0/0"
                            gateway_id = module.ig.ig_id
                            rt_name = "${var.rt_name}_public_rt"
                            subnets = module.subnet.public_subnet_ids
                            require_nat_gateway = false
                            require_gateway = true
                    },
                1 = {
                            cidr_block = "0.0.0.0/0"
                            gateway_id = module.nat.nat_id
                            rt_name = "${var.rt_name}_private_rt"
                            subnets = module.subnet.private_subnet_ids
                            require_nat_gateway = true
                            require_gateway = false
                    }
            }
vpc_id = module.vpc.vpc_id
route_table = each.value
tags = {
            Name = each.value.rt_name
        }
} 

output "vpc_id" {
  value       = module.vpc.vpc_id
  sensitive   = true
  description = "description"
  depends_on  = []
}

terraform {
  backend "s3" {
    bucket = "resourcestatemanagement"
    key    = "vpc/terraform.tfstate"
    region = "ap-south-1"
  }
}