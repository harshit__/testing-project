output "pub_sub_id" {
  value       = module.subnet.public_subnet_ids
  sensitive   = true
  description = "description"
  depends_on  = []
}

output "pri_sub_id" {
  value       = module.subnet.private_subnet_ids
  sensitive   = true
  description = "description"
  depends_on  = []
}