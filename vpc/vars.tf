variable "cidr_block" {
    type = string
    default = "10.0.0.0/16" 
}
variable "instance_tenancy" {
    type = string
    default = "default" 
}

variable "tags" {
    type =  string
    default = "TestingTerraform" 
}

variable "name" {
    type = string
    default = "Task_vpc"
}

variable "pub_name" {
    type = string
    default = "RundeckPublicSubnet"
  
}
variable "pri_name" {
    type = string
    default = "RundeckPrivateSubnet"
  
}
variable "ig_name" {
    type = string
    default = "RundeckIg"
  
}

variable "ng_name" {
    type = string
    default = "RundeckNg"
  
}
variable "rt_name" {
    type = string
    default = "Rundeck"
  
}

variable "eip_name" {
    type = string
    default = "RundeckServerEip" 
}

variable "region" {
    type = string
    default = "ap-south-1" 
}


