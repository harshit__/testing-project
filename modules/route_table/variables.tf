variable "vpc_id" {
  type        = string
  description = "VPC ID for route table"
}

variable "route_table" {
  type        = object({    cidr_block = string
                            require_gateway = bool
                            require_nat_gateway = bool
                            gateway_id = string
                            rt_name = string
                            subnets = list(string)
                        
  })
  description = "Map of route tables to be added"
}

variable "tags"{
    default = {}
}
