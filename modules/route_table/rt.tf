resource "aws_route_table" "rt" {
  vpc_id = var.vpc_id
  tags = var.tags
}

resource "aws_route" "ig_routes"{
  count = "${lookup(var.route_table, "require_gateway")}" ? 1 : 0
  route_table_id = aws_route_table.rt.id
  destination_cidr_block = "${lookup(var.route_table, "cidr_block")}"
  gateway_id = "${lookup(var.route_table, "gateway_id")}" 
}

resource "aws_route" "nat_gw_routes"{
  count = "${lookup(var.route_table, "require_nat_gateway")}" ? 1 : 0
  route_table_id = aws_route_table.rt.id
  destination_cidr_block = "${lookup(var.route_table, "cidr_block")}"
  nat_gateway_id = "${lookup(var.route_table, "gateway_id")}" 
}


resource "aws_route_table_association" "rt_association" {
  count = length("${lookup(var.route_table, "subnets")}")
  route_table_id = aws_route_table.rt.id
  subnet_id = element("${lookup(var.route_table, "subnets")}", count.index)
  }
 