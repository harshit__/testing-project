resource "aws_eip" "natgateway_eip" {
  vpc = true
  tags = var.eip_tags
}

resource "aws_nat_gateway" "ng" {
  allocation_id = aws_eip.natgateway_eip.id
  subnet_id     = var.subnet_id
  tags = var.nat_tags
}