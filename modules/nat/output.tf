output "nat_id" {
  value       = aws_nat_gateway.ng.id
  description = "id of nat gateway created"
  depends_on  = []
}