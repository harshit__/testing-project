variable "subnet_id" {
  type        = string
  description = "ID of subnet, to place the nat gateway" 
}

variable "ng_name" {
  type        = string
  description = "Name of the nat gateway"
}

variable "nat_tags"{
  default = {}
}
variable "eip_name" {
  type        = string
  description = "Name of the elastic ip"
}

variable "eip_tags"{
  default = {}
}