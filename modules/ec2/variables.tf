variable "ami" {}

variable "instance_type_server" {}

variable "instance_type_node" {}
variable "key_name_server" {}

variable "key_name_node" {}

variable "vpc" {
  type        = string
  default     = ""
}


variable "subnet_id" {
  type        = string
  default     = ""
}
variable "subnet_id_public" {
  type = string
  default = ""
}

variable "subnet_id_private" {
  type = string
  default = ""
}

variable "security_group_rundeck" {
  type = list(any)
  default = []
}


variable "security_group_nodes" {
  type = list(any)
  default = []
}

variable "server_name" {
  type        = string
  default     = ""
  description = "description"
}
variable "node_name" {
  type        = list(any)
  default     = []
  description = "description"
}

variable "eip_name" {
  type        = string
  description = "Name of the elastic ip"
}

variable "Rundeck_volume" {
  type        = string
  description = "Name of the rundeck volume"
}