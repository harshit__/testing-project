output "public_ip" {
  value       = aws_instance.RundeckServer.public_ip
  description = "eip created"
  depends_on  = []
}

output "Rundeck_volume" {
    value = aws_ebs_volume.Rundeck_volume.id
    depends_on = []
}