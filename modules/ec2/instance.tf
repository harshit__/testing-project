resource "aws_instance" "RundeckServer" {
    subnet_id      = "${var.subnet_id_public}"
    ami = var.ami
    vpc_security_group_ids = var.security_group_rundeck
    key_name = var.key_name_server
    instance_type = var.instance_type_server
    tags = {
        Name = "${var.server_name}"
    }
}
resource "aws_eip" "Rundeck_eip" {
  vpc = true
  instance = aws_instance.RundeckServer.id
  tags =  {
    Name = var.eip_name
  }
}
resource "aws_ebs_volume" "Rundeck_volume" {
  availability_zone = "ap-south-1a"
  size              = 20
  tags = {
    Name = var.Rundeck_volume
  }
}

resource "aws_volume_attachment" "Rundeck_volume_attach" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.Rundeck_volume.id
  instance_id = aws_instance.RundeckServer.id
}



resource "aws_instance" "Nodes" {
    count  = length(var.node_name)
    subnet_id      = var.subnet_id_private
    vpc_security_group_ids = var.security_group_nodes
    ami = var.ami

    key_name = var.key_name_node
    instance_type = var.instance_type_node
    tags = {
        Name = "${element(var.node_name, count.index)}"
    }
}