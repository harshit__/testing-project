output "public_subnet_ids" {
  value       = aws_subnet.public_subnet.*.id
  description = "List of IDs of public subnets created"
  depends_on  = []
}

output "private_subnet_ids" {
  value       = aws_subnet.private_subnet.*.id
  description = "List of IDs of private subnets created"
  depends_on  = []
}