variable "pub_subnets"{
  type        = list
  default     = []
  description = "list of maps of public subnets"
}

variable "pri_subnets"{
  type        = list
  default     = []
  description = "list of map of private subnets"
}

variable "pub_name" {
  type = string
  default = ""
}
variable "pri_name" {
  type = string
  default = ""
}

variable "vpc_id"{
  type        = string
  description = "vpc id to create subnet in"
}