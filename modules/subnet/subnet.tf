resource "aws_subnet" "public_subnet" {
  vpc_id                  = var.vpc_id
  count                   = length(var.pub_subnets)
  cidr_block              = "${lookup(var.pub_subnets[count.index], "subnet_cidr")}"
  availability_zone       = "${lookup(var.pub_subnets[count.index], "availability_zone")}"
  map_public_ip_on_launch = true
  tags = {
    Name = "${lookup(var.pub_subnets[count.index], "name")}-public-subnet"
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id                  = var.vpc_id
  count                   = length(var.pri_subnets)
  cidr_block              = "${lookup(var.pri_subnets[count.index], "subnet_cidr")}"
  availability_zone       = "${lookup(var.pri_subnets[count.index], "availability_zone")}"
  map_public_ip_on_launch = false
  tags = {
    Name = "${lookup(var.pri_subnets[count.index], "name")}-private-subnet"
  }
}