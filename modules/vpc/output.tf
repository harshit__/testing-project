output "vpc_id" {
  value       = aws_vpc.vpc.id
  sensitive   = true
  description = "ID of generated VPC"
  depends_on  = []
}