variable "vpc_id" {
  type        = string
  description = "vpc_id to add ig"
}

variable "ig_name" {
  type        = string
  description = "Name for the internet gateway"
}

variable "tags"{
  default = {}
}