output "ig_id" {
  value       = aws_internet_gateway.ig.id
  sensitive   = true
  description = "id of created internet gateway"
  depends_on  = []
}