variable "security_group_id_rundeck" {
  type        = string
  default     = ""
  description = "description"
}

variable "security_group_id_nodes" {
  type        = string
  default     = ""
  description = "description"
}
variable "description" {
  type        = string
  default     = ""
  description = "description"
}

variable "tcp_ports_rundeck" {
  type        = list(any)
  default     = []
  description = "tcp_ports"
}
variable "tcp_ports_nodes" {
  type        = list(any)
  default     = []
  description = "tcp_ports"
}

variable "cidrs_rundeck" {
  type    = list(any)
  default = []
}

variable "cidrs_nodes" {
  type    = list(any)
  default = []
}