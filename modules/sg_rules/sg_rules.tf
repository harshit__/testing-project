resource "aws_security_group_rule" "tcp" {
  count             = var.tcp_ports_rundeck == "default_null" ? 0 : length("${var.tcp_ports_rundeck}")
  type              = "ingress"
  from_port         = "${element(var.tcp_ports_rundeck, count.index)}"
  to_port           = "${element(var.tcp_ports_rundeck, count.index)}"
  protocol          = "tcp"
  cidr_blocks       = var.cidrs_rundeck
  description       = var.description
  security_group_id = var.security_group_id_rundeck
}

resource "aws_security_group_rule" "tcp1" {
  count            = var.tcp_ports_nodes == "default_null" ? 0 : length("${var.tcp_ports_nodes}")
  type              = "ingress"
  from_port         = "${element(var.tcp_ports_nodes, count.index)}"
  to_port           = "${element(var.tcp_ports_nodes, count.index)}"
  protocol          = "tcp"
  cidr_blocks       = var.cidrs_nodes
  description       = var.description
  security_group_id = var.security_group_id_nodes
}
