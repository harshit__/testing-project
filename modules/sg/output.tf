output "sg_id_rundeck" {
  value = aws_security_group.sg_rundeck.*.id
}

output "sg_id_nodes" {
  value = aws_security_group.sg_nodes.*.id
}
