resource "aws_security_group" "sg_rundeck" {
  vpc_id      = var.vpc_id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.sg_name_rundeck}"
  }
}

resource "aws_security_group" "sg_nodes" {
  vpc_id      = var.vpc_id
  # egress {
  #   from_port   = 0
  #   to_port     = 0
  #   protocol    = -1
  #   cidr_blocks = ["0.0.0.0/0"]
  # }
  tags = {
    Name = "${var.sg_name_nodes}"
  }
}