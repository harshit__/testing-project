variable "bucket" {
  type        = string
  default     = "resourcestatemanagement"
}
variable "acl" {
    type = string
    default = "private"
}

variable "region" {
  type = string
  default = "ap-south-1"
}
